// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FloppyFightersCharacter.generated.h"

UCLASS(config = Game)
class AFloppyFightersCharacter : public ACharacter
{
	GENERATED_BODY()

		/** Follow camera */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class ACameraActor* Camera;

public:
	AFloppyFightersCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UFUNCTION(BlueprintCallable, Category = "Start")
		void SetDefaults(ACameraActor* Cam);

	UFUNCTION(BlueprintCallable)
	void forceToHoldCharacterUp(FName BoneName, float ForceAmount);
	
	UFUNCTION(BlueprintCallable)
	void deathAction(UParticleSystemComponent* DeathPS, FVector DeathForce, TSubclassOf<UCameraShake> ShakeName, UPhysicsAsset * deathPhys, USoundBase* death);

	UFUNCTION(BlueprintCallable)
	void PlayerState(UParticleSystemComponent* DeathPS, TSubclassOf<UCameraShake> ShakeName, UPhysicsAsset * deathPhys, USoundBase* death);

	UFUNCTION(BlueprintCallable)
	void Impact(AActor * objectHit, UParticleSystemComponent* HitPS, UStaticMeshComponent* OverlapObject, TSubclassOf<UCameraShake> ShakeName, USoundBase* HitSound,  float DamageAmount = 1.f);
	UFUNCTION(BlueprintCallable)
	void Kill();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Health)
		float Health;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Health)
		bool isDead;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Death)
		FVector DeathForce;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Time)
		float SoundTimer;

protected:

	
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	class UCapsuleComponent* MyCap;
	class USkeletalMeshComponent* MyMesh;
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	//public:
	//	/** Returns FollowCamera subobject **/
	//	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return Camera; }
};

