// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FloppyFightersCharacter.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraActor.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Camera/CameraShake.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
//////////////////////////////////////////////////////////////////////////
// AFighterCharacter

AFloppyFightersCharacter::AFloppyFightersCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

}

//////////////////////////////////////////////////////////////////////////
// Input

void AFloppyFightersCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFloppyFightersCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFloppyFightersCharacter::MoveRight);

}

// Sets default values and gets references to self
void AFloppyFightersCharacter::SetDefaults(ACameraActor* Cam)
{
	Camera = Cam;
	MyCap = GetCapsuleComponent();
	MyMesh = GetMesh();
	Health = 100;
	isDead = false;

}
//Adds force to the head making him bobble
void AFloppyFightersCharacter::forceToHoldCharacterUp(FName BoneName, float ForceAmount) //
{
	if (MyMesh != nullptr && MyCap != nullptr)
	{
		FVector force = MyCap->GetUpVector() * ForceAmount;
		FVector ForceToSet = FVector(force.X, force.Y, force.Z);
		MyMesh->AddForce(force, BoneName);
	}
}
//Sets physics body and activates physics on capsule
void AFloppyFightersCharacter::deathAction(UParticleSystemComponent* DeathPS, FVector DeathForce, TSubclassOf<UCameraShake> ShakeName, UPhysicsAsset * deathPhys, USoundBase* death) // Called to set Death Values (Ragdoll, launch force, etc)
{
	if (DeathPS != nullptr && deathPhys != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), death, GetActorLocation(), GetActorRotation(), 1);

		MyCap->SetSimulatePhysics(true);
		MyCap->AddImpulse(DeathForce);	//launches the dead player with the direction between the two
		MyMesh->SetPhysicsAsset(deathPhys, false);
		MyMesh->SetSimulatePhysics(true);
		DeathPS->Activate(true);
		UGameplayStatics::PlayWorldCameraShake(this, ShakeName, GetActorLocation(), 0, 1000, 1);
	}
	
}

void AFloppyFightersCharacter::PlayerState(UParticleSystemComponent * DeathPS, TSubclassOf<UCameraShake> ShakeName, UPhysicsAsset * deathPhys, USoundBase* death) // Called in BP (can be called in c++ tick or anywhere) To update Player
{
	//alive state
	if (Health > 0)
	{
		forceToHoldCharacterUp(FName("head"), 70000);
		SoundTimer -= GetWorld()->DeltaTimeSeconds;
	}
	else //dead state
	{
		if (isDead == false)
		{
			deathAction(DeathPS, DeathForce, ShakeName, deathPhys,death);
			isDead = true;
		}
	}
}

void AFloppyFightersCharacter::Impact(AActor * objectHit, UParticleSystemComponent * HitPS, UStaticMeshComponent * OverlapObject, TSubclassOf<UCameraShake> ShakeName, USoundBase* HitSound, float DamageAmount) // Fist collision (called in BP can be called in event overlap C++0
{
	if (objectHit)
	{
		if (objectHit != this && isDead == false)// checks to see who the object hits and sees if he is dead or not
		{
			AFloppyFightersCharacter* otherPlayer = Cast<AFloppyFightersCharacter>(objectHit);
			if (otherPlayer != nullptr)
			{
				if (SoundTimer <= 0)
				{
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, otherPlayer->GetActorLocation(), otherPlayer->GetActorRotation(), .8f, .8f);
					SoundTimer = .2f;
				}
				otherPlayer->Health -= DamageAmount;
				HitPS->Activate(true);
				otherPlayer->DeathForce = FVector(MyCap - otherPlayer->MyCap).GetSafeNormal() * -100000; //sets the force to launch the other player
				UGameplayStatics::PlayWorldCameraShake(this, ShakeName, GetActorLocation(), 0, 1000, 1);
			}
		}
	}
}

void AFloppyFightersCharacter::Kill() // called to kill characters
{
	isDead = true;
	Health = -1;
}


void AFloppyFightersCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AFloppyFightersCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AFloppyFightersCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFloppyFightersCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFloppyFightersCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		if (Camera == nullptr)
		{
			return;
		}
		const FVector Direction = Camera->GetActorForwardVector();//move in relation to camera rotation
		AddMovementInput(Direction, Value);
	}
}

void AFloppyFightersCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		if (Camera == nullptr)
		{
			return;
		}
		const FVector Direction = Camera->GetActorRightVector();//move in relation to camera rotation
																// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
