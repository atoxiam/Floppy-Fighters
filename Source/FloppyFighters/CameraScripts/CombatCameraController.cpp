// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatCameraController.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "FloppyFightersCharacter.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
//void ACombatCameraController::BeginPlay() //Not Used
//{
//	SetDefaults();
//}

//void ACombatCameraController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) // NotUsed
//{
//	Super::Tick(DeltaTime);
//}

void ACombatCameraController::SetDefaults() // Called in blueprint to set base defaults of camera (If called in c++ begin play you would use PlayerOne and PlayerTwo)
{
	TArray<AActor*> list;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFloppyFightersCharacter::StaticClass(), list);
	
	MidPoint.Z = 5000; //resets height

	if (list[0] != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *list[0]->GetName());
	}
	else
	{
		return;
	}

	PlayerOne = list[0];
	PlayerTwo = list[1];
}

AActor* ACombatCameraController::SetClosestPlayer(AActor * A, AActor * B) //Finds the closest player to the camera and returns a pointer to the actor
{
	float LengthToA = FVector::Dist(GetActorLocation(), A->GetActorLocation());
	float LengthToB = FVector::Dist(GetActorLocation(), B->GetActorLocation());
	
	if (LengthToA > LengthToB)
	{
		return B;
	}
	else
	{
		return A;
	}
	return nullptr;
}



void ACombatCameraController::moveToCombatPoint(AActor* A, AActor* B) // Takes in two actors
{
	ClosestPlayer = SetClosestPlayer(A, B); //Finds the closest Player and assigns it
	if (ClosestPlayer == nullptr) { return; }


	FMath::Clamp(LerpAmount, .01f, .05f);

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->ProjectWorldLocationToScreen(ClosestPlayer->GetActorLocation(), ScreenPoint); // Gets the Screenpoint of the closest player

	if (ScreenPoint.X > .8f || ScreenPoint.X < .2f) // Lerps faster if player is closer to edge of screen
	{
		LerpAmount = GetWorld()->DeltaTimeSeconds * 2.5f;
	}
	else // Lerps slower if the Player is closer towards the middle of the screen
	{
		LerpAmount = GetWorld()->DeltaTimeSeconds;
	}
																			///////////////
		FVector AngleToProject = (GetActorLocation() - MidPoint).GetSafeNormal() * 400; // - Gets the angle between The midpoint and the cameras location and projects 400 units in that direction
		FVector Position = AngleToProject + ClosestPlayer->GetActorLocation();			// - Adds the Players Position to the AngleToProject which gets a point 400 units 
																						//   behind the player that will always have the player between the cam and midpoint
																						///////////////
		float NewXLocation = 0;															
		float NewYLocation = 0; 

		if (FVector::Dist(A->GetActorLocation(), B->GetActorLocation()) > 410)			// If the DIstance between the two Players is greater then 410 units then it lerp the X and Y faster
		{
			NewXLocation = FMath::Lerp(GetActorLocation().X, Position.X, LerpAmount);
			NewYLocation = FMath::Lerp(GetActorLocation().Y, Position.Y, LerpAmount);
		}
		else																		   // else - then it will lerp slowly which will keep the combat jumpy as well as allow for a 
		{																			   //		 smooth transition between closest player
			NewXLocation = FMath::Lerp(GetActorLocation().X, Position.X, LerpAmount/2);
			NewYLocation = FMath::Lerp(GetActorLocation().Y, Position.Y, LerpAmount/3);
		}

		SetActorLocation(FVector(NewXLocation, NewYLocation, MidPoint.Z + 200));       // Sets the camera location to the new X and Y values that are being lerped by time and their set value

}

void ACombatCameraController::moveToOutOfCombatPoint(AActor * A, AActor * B) // not used
{
	return;
}

void ACombatCameraController::oneOnOneCombatMovement(AActor * A, AActor * B) // Calls functions required for 1v1 combat (Called in BP could be called in Event tick c++)
{
	
		PointBetweenTwoActors(A, B); // Gets the point between actors to set our midpoint value
		CameraLookController();      // Cam looks at midpoint
		moveToCombatPoint(A, B);     // Moves to combat point on its own
	
}

FVector ACombatCameraController::PointBetweenTwoActors(AActor * A, AActor * B)
{
	FVector Direction = (A->GetActorLocation() - B->GetActorLocation()); // not used
	FVector Distance = (A->GetActorLocation() + B->GetActorLocation()) / 2; // Gets the midpoint between two actors
	MidPoint = Distance; //sets midpoint
	return Distance; // returns value in case it is called in blueprint
}

void ACombatCameraController::CameraLookController() // looks at midpoint
{

	MidPoint = PointBetweenTwoActors(PlayerOne, PlayerTwo);
	this->SetActorRelativeRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), MidPoint)); // sets rotation to look at midpoint
}
