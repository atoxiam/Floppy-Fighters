# Floppy Fighters 
Floppy Fighters is game made for a 2 week production project on a team with other programmers for an AIE assessment
The main goal of the project was to learn more about working on a team
The game consists of two players who fight by throwing their arms at each other till either one dies or falls off the edge

All art assets besides the boxing gloves were from the Infinity Blade asset packs off the Unreal Marketplace

We do not own the music in the game however the sound effects like the crowd, announcer, and punching sounds
were recorded by Xavier Melton with help from AIE students and staff

[our release is available on itch.io here](https://atoxiam.itch.io/floppy-fighters)

# Controls:
* Needs two Xbox controllers plugged in 
* X and B to attack. 
* Right Trigger to dodge.
* Left Stick To Move
* Use mouse to select menu options

# Team Members:
* Pearson Lawrence
* Xavier Melton
* Matthew Dowling 
* Josh Livingston